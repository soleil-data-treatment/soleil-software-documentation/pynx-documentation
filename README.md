# Aide et ressources de Pynx pour Synchrotron SOLEIL

## Résumé
- Analyse les données de diffractions cohérentes
- Open source

## Sources
- Code source: http://ftp.esrf.fr/pub/scisoft/PyNX/
- Documentation officielle: http://ftp.esrf.eu/pub/scisoft/PyNX/doc/

## Navigation rapide
| Wiki SOLEIL | Tutoriaux | Page pan-data |
| ------ | ------ | ------ |
| [Tutoriel d'installation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/pynx-documentation/-/wikis/Setup-guide) | [Tutoriaux officiels](http://ftp.esrf.eu/pub/scisoft/PyNX/doc/tutorial/index.html) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/102/pynx) |
| [Tutoriaux SOLEIL](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/pynx-documentation/wikis/home) | | |

## Installation
- Systèmes d'exploitation supportés: Linux, MacOS, python
- Installation: Facile (tout se passe bien), Relativement facilement sur debian

## Format de données
- en entrée: .cxdi
- en sortie: .hdf5
- sur la Ruche locale
